<?php
/**
 * Plugin Name:       Breadcrumbs
 * Plugin URI:        https://bitbucket.org/lewisdorigo/breadcrumbs
 * Description:       A simple WordPress class to generate and return breadcrumbs.
 * Version:           1.0.0
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */

namespace Dorigo\Breadcrumbs;

class Breadcrumbs {
    private $object;
    private $type;
    private $breadcrumbs;
    private $hasACF;

    public function __construct($object = null) {
        if(!$object && is_404()) {
            $this->breadcrumbs = $this->notFound();
            return;
        }

        $object = $object ?: get_queried_object();
        $this->object = !is_object($object) ? get_post($object) : $object;

        $this->type = get_class($object);

        $hasACF = function_exists("get_field");

        switch($this->type) {
            case "WP_Term":
                $this->breadcrumbs = $this->taxonomy();
                break;
            case "WP_User":
                $this->breadcrumbs = $this->user();
                break;
            case "WP_Post_Type":
                $this->breadcrumbs = $this->postType();
                break;
            default:
                $this->breadcrumbs = $this->post();
                break;
        }
    }

    public function get() {
        return $this->breadcrumbs;
    }

    private function postType($object = null) {
        $object = $object ?: $this->object;

        switch($object->name) {
            case 'product':
                $archive = get_option('woocommerce_shop_page_id') ?: false;
                break;
            case 'post':
                $archive = get_option('page_for_posts') ?: false;
                break;
            default:
                $archive = $this->hasACF ? get_field("{$object->name}_archive","options") : null;
                break;
        }


        if($archive && ($post = get_post($archive))) {
            return $this->post($post);
        }

        $links = [
            [
                'title' => $object->labels->name,
                'url' => get_post_type_archive_link($object->name),
            ]
        ];

        $home = $this->getHome();
        array_unshift($links, ...$home);

        return $links;

    }

    private function post($post = null) {
        $post = $post ?: $this->object;

        $links = [];

        $posts = get_post_ancestors($post->ID);
        $posts = array_reverse($posts);

        switch($post->post_type) {
            case 'post':
                $archive = get_option('page_for_posts') ?: null;
                break;
            case 'product':
                $archive = get_option('woocommerce_shop_page_id') ?: null;
                break;
            default:
                $archive = $this->hasACF ? get_field("{$post->post_type}_archive","options") : null;
                break;
        }

        if($archive) {
            array_unshift($posts, $archive);

            $archive_ancestors = get_post_ancestors($archive);
            $archive_ancestors = array_reverse($archive_ancestors);

            if($archive_ancestors) {
                array_unshift($posts, ...$archive_ancestors);
            }
        }

        array_push($posts, $post->ID);

        foreach($posts as $post_id) {

            if($post->ID === (int) get_option("page_on_front")) {
                break;
            }

            $links[] = [
                'title' => get_the_title($post_id),
                'url' => get_permalink($post_id),
            ];

        }

        $home = $this->getHome();
        array_unshift($links, ...$home);

        return $links;
    }

    private function taxonomy($object = null) {
        $object = $object ?: $this->object;

        $links = [];
        $hasHome = false;

        global $wp_taxonomies;

        if(isset($wp_taxonomies[$object->taxonomy])) {
            $postType = $wp_taxonomies[$object->taxonomy]->object_type[0];

            if($postType) {
                $postType = get_post_type_object($postType);
            }

            $links = $postType ? $this->postType($postType) : $links;
            $hasHome = true;
        }


        if(!$hasHome) {
            $home = $this->getHome();
            array_unshift($links, ...$home);
        }

        array_push($links, [
            'title' => $object->name,
            'url'   => get_term_link($object, $object->taxonomy),
        ]);

        return $links;
    }

    private function getHome() {
        $links = [];

        $links[] = [
            'title' => 'Home',
            'url'   => network_site_url('/'),
            'rel'   => ['home','index'],
        ];

        if(network_site_url('/') !== home_url('/')) {
            $links[] = [
                'title' => get_bloginfo('name'),
                'url'   => home_url('/'),
                'rel'   => ['home','index'],
            ];
        }

        return $links;
    }

    private function notFound() {
        $links = $this->getHome();

        array_push($links, [
            'title' => 'Not Found',
            'url' => ''
        ]);

        return $links;
    }
}
